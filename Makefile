
PYTHON ?= python
TOUCH  ?= touch
PREFIX ?= /usr/local

CP      = cp
CP_P    = $(CP) -p
RM      = rm
RM_F    = $(RM) -f
RM_FR   = $(RM_F) -r
AWK     = awk
MKDIR_P = mkdir -p
INSTALL = install -p -D
INSTALL_REG = $(INSTALL) -m 644
INSTALL_EXE = $(INSTALL) -m 755
INSTALL_DIR = $(MKDIR_P) -m 755

SOURCE_DIR = src

BUILD_PRE = build
BUILD_BIN = $(BUILD_PRE)/bin
BUILD_OK  = $(BUILD_PRE)/all.ok

BUILD_EXE_TARGETS = \
	wombat \
	lr2mol \
	molnorm

BUILD_ENV_TARGETS = \
	activate

INSTALL_PRE = $(PREFIX)
INSTALL_BIN = $(PREFIX)/bin

INSTALL_EXE_TARGETS = \
	$(INSTALL_BIN)/wombat \
	$(INSTALL_BIN)/lr2mol \
	$(INSTALL_BIN)/molnorm

INSTALL_ENV_TARGETS = \
	$(INSTALL_PRE)/activate

TEST_DIR = test
TEST_TARGETS = \
	$(BUILD_PRE)/wombat.ok

PACKAGE_VERSION := $(shell git describe)


.PHONY: all activate test check install uninstall clean

all: $(BUILD_EXE_TARGETS) $(BUILD_ENV_TARGETS)

$(BUILD_EXE_TARGETS): %: $(BUILD_BIN) $(BUILD_BIN)/%

$(BUILD_BIN)/%: $(SOURCE_DIR)/%.py
	$(AWK) -v version=$(PACKAGE_VERSION) '{sub("__PACKAGE_VERSION__",version); print}' $(SOURCE_DIR)/$*.py >$@

$(BUILD_BIN):
	$(INSTALL_DIR) $@

$(BUILD_ENV_TARGETS): %: $(BUILD_BIN) $(BUILD_PRE)/%s

$(BUILD_PRE)/%:
	echo 'export PATH=$(INSTALL_BIN):$$PATH' >$@

install: $(BUILD_OK) $(INSTALL_BIN) $(INSTALL_EXE_TARGETS) $(INSTALL_ENV_TARGETS)

$(INSTALL_BIN)/%: $(BUILD_BIN)/%
	$(INSTALL_EXE) -t $(INSTALL_BIN) $^

$(INSTALL_BIN):
	$(INSTALL_DIR) $@

$(INSTALL_PRE)/%: $(BUILD_PRE)/%
	$(INSTALL_REG) -t $(INSTALL_PRE) $^

uninstall:
	-$(RM_F) $(INSTALL_EXE_TARGETS) $(INSTALL_REG_TARGETS)

test: check

check: $(BUILD_OK)

$(BUILD_OK): $(BUILD_BIN) $(TEST_TARGETS)
	$(TOUCH) $(BUILD_OK)

$(TEST_TARGETS): $(BUILD_PRE)/%.ok: $(TEST_DIR)/test_%.py
	@export PYTHONPATH='$(CURDIR):$(SOURCE_DIR):$(PYTHONPATH)'; \
	$(PYTHON) -m unittest -v $(TEST_DIR).$(shell basename $< .py) && \
	$(TOUCH) $@

clean:
	-$(RM_FR) $(BUILD_PRE)

