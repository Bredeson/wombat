**WOMBAT**
==========

DESCRIPTION
-----------
wombat - **W**eak **O**verlap **M**is-assembly **B**reaking **A**nd **T**rimming

INSTALL
-------
The following non-standard Python libraries must first be installed:
* pysam
* scipy
* matplotlib

The user may then clone the WOMBAT repo from GitLab and build it using `make`:
```sh
$ git clone git@gitlab.com:Bredeson/wombat.git
$ cd wombat
$ make PREFIX=/full/path/to/install/prefix
$ make test
$ make install PREFIX=/full/path/to/install/prefix
$ make clean
```

An `activate` file is also created and installed in the `PREFIX` directory,
this is to allow the user to easily configure their environment if `PREFIX`
defines a non-standard install location (*e.g.*, `/usr/local`):
```sh
$ source /full/path/to/install/prefix/activate
```


OPTIONS
-------
```
Options:
  -L,--list <str>|<file>    Contig name or .list file of contig names,
                            listing one contig per line. May be specified
                            zero or more times. default: all contigs

  -l,--sample-limit <uint>  Limit param inference to the first N windows
                            on the longest contig. default: all

  -A,--span-alpha <float>   Lower-tail alpha for the number of molecules
                            spanning the target window. Windows with
                            spanning depth below this value trigger term
                            window scanning. default: 0.01

  -a,--term-alpha <float>   Upper-tail alpha for the number of molecules
                            starting/ending within the target window.
                            Regions with excess starts/ends are annotated
                            for breaking. default: 0.01

  -W,--span-width <uint>    Sliding window width for counting spanning
                            molecules. 1kb or larger recommended.
                            default: auto

  -S,--span-step <uint>     Sliding window step size for counting spanning
                            molecules. default: 0.5 * span-width

  -w,--term-width <uint>    Sliding window width for counting molecule 
                            starts/ends. 100 bp or smaller recommended.
                            default: auto

  -s,--term-step <uint>     Sliding window step size for counting molecule
                            starts/ends. default: 0.5 * term-width
                            
  -F,--fit-only             Only perform parameter estimation by fitting
                            and then exit.

  -B,--no-break             Do not perform any contig breaking

  -T,--no-trim              Do not perform any contig trimming

  -D,--debug                Run in debugging mode (very verbose)

  -h,--help                 Print this help documentation and exit

  -v,--version              Print the version and exit
```

>  reference.fasta contains contigs and is indexed with samtools faidx.
>  molecule.bed.gz is a bgzip-compressed and tabix-indexed BED file of
>  aligned molecules: If PacBio/ONT long reads, the BED records represent
>  longest single subreads (only 10kb+ recommended); if 10X Genomics data,
>  each BED record must represent the extents of each GEM-barcoded molecule
>  as mapped to the reference, not the individual linked reads; if mate-
>  pairs (experimental), each BED record represents the outer-coordinates
>  of the mapped molecule/insert.

>  The `--term-alpha` and `--span-alpha` options are used to estimate molecule
>  count thresholds for significant deviations from the expected number of
>  molecules starting/ending and spanning an interval window, respectively,
>  based on their fitted distributions. If these options are set to values
>  greater than or equal to 1.0, no empirical estimation is performed and
>  those values are used directly as the maximum starting/ending and min-
>  imum spanning molecule count thresholds. This is useful if the parameter
>  estimation procedure fails to determine reasonable values because of
>  poorly fit data.


INPUTS
------
reference.fasta contains contigs and is indexed with samtools faidx

molecule.bed.gz is a bgzip-compressed and tabix-indexed BED file of
aligned molecules:  


**Using SMS long-reads**:
If PacBio/ONT long reads, the BED records should represent the longest
single subreads (only 10kb+ recommended). Starting with a coordinate-
sorted BAM:
```sh
$ bedtools bamtobed -i longread.bam \
    | awk 'OFS="\t" {if ($3-$2 >= 10000) { print }}' \
    | bgzip >longread.bed.gz

$ tabix -p bed longread.bed.gz
```

**Using mate-pairs**:
If mate-pairs (EXPERIMENTAL), each BED entry represents the outer
coordinates of the molecule/insert. Starting with a readname-
sorted BAM:
```sh
$ samtools view -b -f3 -F3852 matepair.bam \
    | bedtools bamtobed -bedpe -i - \
    | awk 'OFS="\t" {print $1,$2,$6,$7,$8,"+"}' \
    | sort -k1,1 -k2,2n -k3,3n \
    | bgzip >matepair.bed.gz

$ tabix -p bed matepair.bed.gz
```

**Using 10X Genomics linked-reads**:
If 10X Genomics data, each BED record must represent the extents of each
GEM-barcoded molecule as mapped to the reference, not the individual
linked reads.
```sh
# Assuming the user has possorted_bam.bam output from longranger:
$ lr2mol -D 50000 -l 10000 -q 10 possorted_bam.bam >molecules.bed
$ molnorm -D 50 -L 50000 -w 1000 molecules.bed reference.fasta \
    |  bgzip >molecules.bed.gz \
    && tabix -p bed molecules.bed.gz 
```

OUTPUTS
-------

|      File name        |               Description                | 
|-----------------------|------------------------------------------|
|  out_prefix.span.tsv  |  Spanning molecule counts used by parameter estimation procedure to determine `-A` value |
|  out_prefix.span.pdf  |  Spanning molecule count histogram with fit line parameterized by estimated values |
|  out_prefix.freq.tsv  |  Spanning molecule fractions used by parameters estimation procedure to determine `-f` value |
|  out_prefix.freq.pdf  |  Spanning molecule fraction histogram with fit line parameterized by estimated values |
|  out_prefix.term.tsv  |  Molecule termini counts used by parameter estimation procedure to determine `-a` value |
|  out_prefix.term.pdf  |  Molecule termini count histogram with fit line parameterized by estimated values |
|  out_prefix.fasta     |  FASTA file of broken and trimmed contigs |
|  out_prefix.bed       |  BED file of breaks, trims, and clear regions formatted for viewing in IGV |


**NOTE: It is highly recommended that the user test a range of
parameter values on their input dataset using a few contigs, then
visualize the data and WOMBAT output in IGV to ensure that WOMBAT
is behaving sensibly.**


VERSION
-------
0.1.11

AUTHOR
------
Jessen V. Bredeson <jessenbredeson@berkeley.edu>
