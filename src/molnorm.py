#!/usr/bin/env python

import os
import sys
import gzip
import math
import getopt
import logging

__author__  = 'Jessen Bredeson <jessenbredeson@berkeley.edu>'
__version__ = '__PACKAGE_VERSION__'
__program__ = os.path.basename(sys.argv[0]) if __name__ == '__main__' else __name__

name = 0
beg = 1
end = 2


# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)

try:
    # importing non-standard Python libraries:
    import pysam
except ImportError as error:
    logging.error("%s: %s" % (error.__class__.__name__, str(error)))
    sys.exit(1)



def _log_exception(exception, message, traceback):
    """
Exception handler hook to redirect errors to be reported
by the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)

    

def ceil(value):
    ivalue = int(value)
    return ivalue+1 if value-ivalue > 0 else ivalue



def by_length(record):
    return record[beg] - record[end]  # sort by longest to shortest



def normalize(records, ref_name, ref_length, max_depth, max_length, bin_width, outstream=sys.stdout):
    normalized_records = []
    bin_depth = [0] * (ref_length // bin_width + 1)
    for record in sorted(records, key=by_length):
        if record[end]-record[beg] > max_length:
            continue
        place_read = False
        bin_beg = record[beg]//bin_width
        bin_end = record[end]//bin_width
        for b in range(bin_beg, bin_end+1):
            if bin_depth[b] < max_depth:
                place_read = True
                break
        
        if place_read:
            # calculate fraction of start overlapping bin_beg
            #    record_beg o-----------S  
            # bin_beg +-----------------+ bin_beg+1
            bin_depth[bin_beg] += float((bin_beg+1)*bin_width - record[beg]) / bin_width

            # calculate fraction of start overlapping bin_beg
            #         S--------o record_end
            # bin_end +-----------------+ bin_end+1
            bin_depth[bin_end] += float(record[end] - bin_end*bin_width) / bin_width

            for b in range(bin_beg+1, bin_end):
                bin_depth[b] += 1
            
            normalized_records.append(record)
        
    for record in sorted(normalized_records):
        outstream.write('\t'.join(map(str, record)) + '\n')
            
                

def normalize_depth(ifile, ofile, ref_lengths, max_depth, max_length, bin_width=1000):
    prev_ref_name = None
    prev_ref_records = []
    for line in ifile:
        curr_ref_record = line.rstrip().split()
        
        curr_ref_record[beg] = int(curr_ref_record[beg])
        curr_ref_record[end] = int(curr_ref_record[end])
        
        if curr_ref_record[name] != prev_ref_name:
            if prev_ref_name is not None:
                normalize(prev_ref_records, prev_ref_name, ref_lengths[prev_ref_name], max_depth, max_length, bin_width, outstream=ofile)
            prev_ref_records = []
            prev_ref_name = curr_ref_record[name]
        
        prev_ref_records.append(curr_ref_record)

    if prev_ref_name is not None:
        normalize(prev_ref_records, prev_ref_name, ref_lengths[prev_ref_name], max_depth, max_length, bin_width, outstream=ofile)



def usage(message=None, retcode=1):
    message = '' if message is None else 'Error: %s\n\n' % message
    sys.stderr.write("\n")
    sys.stderr.write("Usage:   %s [options] <in.bed> <genome.fa>\n" % (__program__))
    sys.stderr.write("\n")
    sys.stderr.write("Options: -o <file>     Write output to file [stdout]\n")
    sys.stderr.write("         -D <uint>     Target depth to normalize to [2^%d-1]\n" % (
        int(math.log(sys.maxsize)/math.log(2)+1)))
    sys.stderr.write("         -L <uint>     Max molecule length [2^%d-1]\n" % (
        int(math.log(sys.maxsize)/math.log(2)+1)))
    sys.stderr.write("         -w <uint>     Normalization window width [1000]\n")
    sys.stderr.write("         -h,--help     Print this help documentation and exit\n")
    sys.stderr.write("         -v,--version  Print the version and exit\n")    
    sys.stderr.write("\n%s" % message)
    sys.stderr.write("\n")
    sys.exit(retcode)


def version():
    sys.stderr.write("%s %s\n" % (__program__.upper(), __version__))
    sys.exit(0)
    

def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'L:D:o:w:hv', ['help','version'])
    except getopt.GetOptionError as message:
        usage(message)

    bin_width = 1000
    max_depth = sys.maxsize
    max_length = sys.maxsize
    outputfile = '-'
    try:
        for flag, value in options:
            if   flag in ('-h','--help'): usage()
            elif flag in ('-v','--version'): version()
            elif flag == '-L': max_length = int(value)
            elif flag == '-D': max_depth = int(value)
            elif flag == '-w': bin_width = int(value)
            elif flag == '-o': outputfile = value
    except ValueError:
        usage("Non-numeric value for %r: %r" % (flag, value))
        
    if len(arguments) != 2:
        usage("Unexpected number of arguments")

    ibed = sys.stdin  if arguments[0] == '-' else gzip.GzipFile(arguments[0], 'r') \
           if arguments[0].endswith('.gz') else open(arguments[0], 'r') 
    obed = sys.stdout if outputfile == '-' else gzip.GzipFile(outputfile, 'w') \
           if outputfile.endswith('.gz') else open(outputfile, 'w')
    ifna = pysam.FastaFile(arguments[1])
    ifai = dict(zip(ifna.references, ifna.lengths))

    normalize_depth(ibed, obed, ifai, max_depth, max_length, bin_width)

    ibed.close()
    obed.close()


if __name__ == '__main__':
    main(sys.argv[1:])

