#!/usr/bin/env python

import os
import sys
import math
import getopt
import logging

__author__  = 'Jessen Bredeson <jessenbredeson@berkeley.edu>'
__version__ = '__PACKAGE_VERSION__'
__program__ = os.path.basename(sys.argv[0]) if __name__ == '__main__' else __name__


# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)


try:
    # importing non-standard Python libraries:
    import pysam
except ImportError as error:
    logging.error("%s: %s" % (error.__class__.__name__, str(error)))
    sys.exit(1)


    
def _log_exception(exception, message, traceback):
    """
Exception handler hook to redirect errors to be reported
by the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)

    

def format_molecules(extents, reference_name, min_length):
    index = {}
    formatted = []
    prev_extent = [0, 0]
    for curr_extent in sorted(extents):
        if curr_extent[1] - curr_extent[0] < min_length:
            # not long enough
            continue
            
        if abs(curr_extent[0] - prev_extent[0]) < 100 and \
           abs(curr_extent[1] - prev_extent[1]) < 100:
            # duplicate
            continue

        if curr_extent[2] in index:
            index[curr_extent[2]] += 1
        else:
            index[curr_extent[2]] = 1

        formatted.append("%s\t%d\t%d\t%s.%d\t%d\t+\n" % (
            reference_name, curr_extent[0], curr_extent[1], curr_extent[2], index[curr_extent[2]], curr_extent[3])
        )
        
        prev_extent = curr_extent

    return ''.join(formatted)



def lr2mol(argv, max_molecule_distance=sys.maxsize, min_molecule_length=10000, min_mapping_quality=0):
    ibam = pysam.AlignmentFile(argv[0])
    # fbam = pysam.AlignmentFile(argv[1] + '.failed.bam', mode='wb', template=ibam)
    obed = sys.stdout  # open(argv[1] + '.extent.bed', 'w')


    molecule_count = 0
    molecule_extent = []
    molecule_index = {}
                   
    previous_reference_name = None
    for record in ibam:
        if record.is_unmapped or \
           record.reference_id < 0:
            continue
            
        if record.is_duplicate or \
           record.is_secondary or \
           record.is_supplementary:
            continue

        if record.mapping_quality < min_mapping_quality:
            continue

        if record.reference_name != previous_reference_name:
            if previous_reference_name is not None:
                obed.write(format_molecules(molecule_extent, previous_reference_name, min_molecule_length))
            previous_reference_name = record.reference_name
            molecule_extent = []
            molecule_index = {}
            molecule_count = 0

        try:
            barcode = record.get_tag('BX', with_value_type=False)
        except KeyError:
            # fbam.write(record)
            continue

        if barcode not in molecule_index:
            molecule_extent.append([record.reference_start, record.reference_end, barcode, 1])
            molecule_index[barcode] = molecule_count
            molecule_count += 1
            
        if record.reference_start - molecule_extent[molecule_index[barcode]][1] <= max_molecule_distance:
            molecule_extent[molecule_index[barcode]][1] = record.reference_end
            molecule_extent[molecule_index[barcode]][3] += 1

        else:
            molecule_extent.append([record.reference_start, record.reference_end, barcode, 1])
            molecule_index[barcode] = molecule_count
            molecule_count += 1
        
    obed.write(format_molecules(molecule_extent, previous_reference_name, min_molecule_length))
    

def version():
    sys.stderr.write("%s %s\n" % (__program__.upper(), __version__))
    sys.exit(0)

    
def usage(message=None, status=1):
    message = '' if message is None else "Error: %s\n\n" % message
    sys.stderr.write("\n")
    sys.stderr.write("Usage:   %s [options] <in.bam>\n" % (__program__))
    sys.stderr.write("\n")
    sys.stderr.write("Options: -D <uint>     Max intra-molecule linked-read distance [2^%d-1]\n" % (
        int(math.log(sys.maxsize)/math.log(2)+1)))
    sys.stderr.write("         -l <uint>     Min linked-read molecule length [10000]\n")
    sys.stderr.write("         -q <uint>     Min read-level mapping quality [0]\n")
    sys.stderr.write("         -h,--help     Print this help documentation and exit\n")
    sys.stderr.write("         -v,--version  Print the version and exit\n")
    sys.stderr.write("\n%s" % message)
    sys.stderr.write("\n")
    sys.exit(status)
    
    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'D:l:q:hv', ['help','version'])
    except getopt.GetoptError as message:
        usage(message)
        
    min_len = 10000
    min_mapq = 0
    max_dist = sys.maxsize
    call_help = False
    try:
        for flag, value in options:
            if   flag in ('-h','--help'): usage()
            elif flag in ('-v','--version'): version()
            elif flag == '-D': max_dist = int(value)
            elif flag == '-q': min_mapq = int(value)
            elif flag == '-l': min_len = int(value)
    except ValueError:
        usage("Non-numeric value for %r: %r" % (flag, value))
        
    if len(arguments) != 1:
        usage('Unexpected number of arguments')

    sys.excepthook = _log_exception
        
    lr2mol(arguments, max_molecule_distance=max_dist, min_molecule_length=min_len, min_mapping_quality=min_mapq)

    
if __name__ == '__main__':
    main(sys.argv[1:])
