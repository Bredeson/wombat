#!/usr/bin/env python
"""
Weak Overlap Mis-assembly Breaking And Trimming
"""

# Copyright (c)2013. The Regents of the University of California (Regents).
# All Rights Reserved. Permission to use, copy, modify, and distribute this
# software and its documentation for educational, research, and
# not-for-profit purposes, without fee and without a signed licensing
# agreement, is hereby granted, provided that the above copyright notice,
# this paragraph and the following two paragraphs appear in all copies,
# modifications, and distributions. Contact The Office of Technology
# Licensing, UC Berkeley, 2150 Shattuck Avenue, Suite 510, Berkeley, CA
# 94720-1620, (510) 643-7201, for commercial licensing opportunities.

# IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
# SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

# REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
# HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
# MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

import os
import sys
import math
import getopt
import logging

__author__  = 'Jessen Bredeson <jessenbredeson@berkeley.edu>'
__version__ = '__PACKAGE_VERSION__'
__program__ = os.path.basename(sys.argv[0]) if __name__ == '__main__' else __name__

_num = len
_freq_min = 0
_span_min = 3
_term_min = 1
_freq_alpha_default = 1e-4
_span_alpha_default = 1e-2
_term_alpha_default = 1e-2
_span_wsize_default = 0
_span_ssize_default = 0
_term_wsize_default = 0
_term_ssize_default = 0
_align_length_default = _span_wsize_default + 2
_sample_limit  = sys.maxsize
_track_header  = "#track itemRgb=\"On\"\n"
_trim_record   = "{0:s}\t{1:d}\t{2:d}\tTRIM\t0\t+\t{1:d}\t{2:d}\t213,221,213\n".format
_clear_record  = "{0:s}\t{1:d}\t{2:d}\tCLEAR\t0\t+\t{1:d}\t{2:d}\t0,0,0\n".format
_break_record  = "{0:s}\t{1:d}\t{2:d}\tBREAK\t0\t+\t{1:d}\t{2:d}\t213,221,213\n".format
_debug_record  = "{0:s}\t{1:d}\t{2:d}\tDEBUG\t0\t+\t{3:d}\t{4:d}\t{5:d}\t{6:.03f}\n".format
_region_string = "{0:s}:{1:d}-{2:d}".format


# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)


try:
    # importing non-standard Python libraries:
    # (module names prefixed with `_' to prevent pydoc API exposure)
    import pysam
    import scipy
    import matplotlib
    import matplotlib.pyplot as _pyplot

    from scipy.stats import gamma as _gamma
    from scipy.stats import poisson as _poisson
    from scipy.stats import logistic as _logistic
    from matplotlib.backends.backend_pdf import PdfPages as _openPDF
except ImportError as error:
    logging.error("%s: %s" % (error.__class__.__name__, str(error)))
    sys.exit(1)



def _log_exception(exception, message, traceback):
    """
Exception handler hook to redirect errors to be reported 
by the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)



def _range(start=0, end=1, by=1):
    roundto = len(str(float(by)).split('.')[1])
    while start < end:
        yield round(start, roundto)
        start += by



def _mean(X, default=None, key=lambda x: x):
    if len(X) < 1:
        return default
    
    E = 0
    N = 0
    for x in X:
        E += key(x)
        N += 1

    return float(E) / max(1, N)



def _sd(X, mean=None, default=None, key=lambda x: x):
    if len(X) < 1:
        return default
    
    if mean is None:
        mean = _mean(X, default=0.0, key=key)

    N = 0
    s = 0
    for x in X:
        s += pow(key(x) - mean, 2)
        N += 1
        
    return pow(s / max(1, N - 1), 0.5)

        
        
def _fold(linear_string, width=None):
    if width is None:
        return linear_string

    folded_string = ''
    string_length = len(linear_string)
    for offset in range(0, string_length, width):
        if offset+width < string_length:
            folded_string += linear_string[offset:offset+width] + "\n"
        else:
            folded_string += linear_string[offset:string_length]
            
    return folded_string



def _fasta_record(name, sequence, width=None):
    """
Format fasta records with/without multi-line wrapping
    """
    return ">%s\n%s\n" % (name, _fold(sequence, width=width))



def _fitgamma(raw_data, alpha=0.01, upper=False, prefix=None, freq=False):
    if len(raw_data) == 0:
        return -1
    if max(raw_data) == 0:
        return -1
    if upper:
        alpha = 1 - alpha

    raw_data.sort()
    raw_median = _median(raw_data)
    max_domain = raw_data[-1]

    # perform an initial fit with the gamma distribution, if the expected
    # value is less than 2, fall back to using the poisson distribution. 
    # SciPy's MLE fit to exponentially distributed data is not very good 
    # with gamma.fit.
    # E(gamma) = shape * scale + location

    shape = 0.0
    location = 0.0
    scale = 0.0
    expected = -1
    function = _gamma
    if freq:
        function = _logistic
        fit_domain = list(_range(0.0, 1.01, 0.01))
        fit_data = list(filter(lambda x: 0.0 < x < 1.0, raw_data))
        num_data = _num(fit_data)

        location, scale = function.fit(fit_data)
        fit_pdf = list(map(lambda x: x*num_data/100.0, function.pdf(fit_domain, location, scale)))
        expected = function.ppf(alpha, location, scale)

    else:  # integer values
        fit_domain = list(range(max_domain+1))
        shape, location, scale = function.fit(raw_data)
        if raw_median < 2:
            function = _poisson
            fit_data = raw_data
            num_data = _num(fit_data)

            shape, location, scale = _mean(fit_data, default=0.0), 0.0, 0.0
            fit_pdf = list(map(lambda x: x*num_data, function.pmf(fit_domain, shape)))
            expected = function.ppf(alpha, shape)

        else:
            min_depth = 10 if raw_median > 30 else 2
            fit_data = list(filter(lambda x: x >= min_depth, raw_data))            
            num_data = _num(fit_data)

            shape, location, scale = function.fit(fit_data)        
            fit_pdf = list(map(lambda x: x*num_data, function.pdf(fit_domain, shape, location, scale)))
            expected = function.ppf(alpha, shape, location, scale)

        try:
            expected = int(round(expected, 0))
        except ValueError:
            expected = -1

    if prefix is not None:
        logging.debug("   Writing %s.tsv" % prefix)

        tsv = open("%s.tsv" % (prefix,), 'w')
        for datum in raw_data:
            tsv.write("%s\n" % str(datum))
        tsv.close()

        if 'DISPLAY' in os.environ:
            logging.debug("   Writing %s.pdf" % prefix)

            pdf = _openPDF("%s.pdf" % (prefix,))
            _pyplot.figure()
            _pyplot.clf()
            _pyplot.hist(raw_data, bins=fit_domain, range=(-0.5,max_domain+0.5), align="left")  # +1 for X==0
            _pyplot.plot(fit_domain, fit_pdf)
            _pyplot.title("%s\nX ~ %s(shape=%.4f, loc=%.4f, scale=%.4f)" % (
                prefix, function.name, shape, location, scale))
            _pyplot.xlabel("Molecule counts", fontsize=13)
            _pyplot.ylabel("Number of windows", fontsize=13)
            pdf.savefig()
            _pyplot.close()
            pdf.close()

        else:
            logging.warn("No display name and no $DISPLAY environment variable, cannot generate plots")

    return expected



def _integrate(data, alpha=0.01, upper=False):
    if len(data) == 0:
        return -1
    if max(data) == 0:
        return -1
    if upper:
        alpha = 1 - alpha

    sum_ = 0
    hist = {}
    total = 0
    for datum in data:
        if datum in hist:
            hist[datum] += 1
        else:
            hist[datum] = 1
        total += 1

    total = float(total)
    for datum in sorted(hist):
        sum_ += hist[datum]
        if sum_ / total >= alpha:
            return datum

    return -1



def _median(X, key=lambda x: x):
    X = sorted(X, key=key)
    N = len(X)

    if N % 2 == 0:
        return((key(X[N//2]) + key(X[N//2+1])) / 2.0)
    else:
        return key(X[N//2])



def _median_median_line(data):
    """Assumes a list of lists or tuples: (contig, length, nreads)"""
    data = sorted(data, key=lambda x: x[1])
    
    # For an alternative approach to robust line fitting, see RANSAC:
    # https://salzis.wordpress.com/2014/06/10/robust-linear-model-estimation-using-ransac-python-implementation/

    partition = len(data) // 3

    # group1 and group3 must be the same size:
    group1 = data[:partition]
    group2 = data[partition:-partition]
    group3 = data[-partition:]

    median_x = (_median(group1, key=lambda x: x[1]),
                _median(group2, key=lambda x: x[1]),
                _median(group3, key=lambda x: x[1]))
    median_y = (_median(group1, key=lambda x: x[2]),
                _median(group2, key=lambda x: x[2]),
                _median(group3, key=lambda x: x[2]))

    slope = (median_y[2] - median_y[0]) / (median_x[2] - median_x[0])
    intercept = (sum(median_y) - slope * sum(median_x)) / 3

    return(intercept, slope)



def calculate_windowed_depth(reads, reference_name, reference_start, reference_end,
                             window_width=100, window_step=50, min_alignment_length=100,
                             limit=_sample_limit, counts=False):
    """
Counts the number of reads spanning or starting/stopping within 
a given interval window.
    """
    logging.debug("   Calculating window (width=%d, step=%d) depth: %s" % (
        window_width, window_step, reference_name))

    iterations = 0
    reference_intervals = []
    reference_num_terms = []
    reference_num_spans = []
    reference_frq_spans = []
    for window_start in range(reference_start, reference_end - window_step + 1, window_step):
        if iterations == limit:
            break
        window_end = min(reference_end, window_start + window_width)
        num_starts = 0
        num_ends = 0
        num_spans = 0
        try:
            window_reads = reads.fetch(reference_name, window_start, window_end)
        except ValueError:  # no reads intersect window
            window_reads = []

        for read in window_reads:
            if read.end - read.start < min_alignment_length:
                continue
            if read.start < window_start and window_end < read.end:
                num_spans += 1
            elif window_start <= read.start and read.end <= window_end:
                continue
            elif window_start <= read.start < window_end:
                num_starts += 1
            elif window_start < read.end <= window_end:
                num_ends += 1
            
        frq_spans = num_spans / max(1.0, num_spans + 0.5 * (num_ends + num_starts))

        if counts:
            reference_num_terms.append(num_starts)
            reference_num_terms.append(num_ends)
            reference_num_spans.append(num_spans)
            reference_frq_spans.append(frq_spans)
        else:
            reference_intervals.append((
                window_start,
                window_end,
                num_ends,
                num_spans,
                num_starts,
                frq_spans)
            )

        iterations += 1
            
    if counts:
        return {
            'span': reference_num_spans,
            'term': reference_num_terms,
            'freq': reference_frq_spans,
            'iter': iterations
        }
    else:
        return reference_intervals



def _set_window_sizes(field, params, reference_length, num_reads, scale=1):
    window_width = '%s_window_width' % field
    window_step = '%s_window_step' % field

    # TODO: Need to revise how to choose these sizes for low depth datasets
    if params.get(window_width, 0) == 0:
        logging.info("  Inferring %s window width and step sizes" % field)
        params[window_width] = max(10, scale * int(reference_length / float(num_reads)))
        params[window_step] = max(1, params[window_width] // 2)
    else:
        logging.info("  Implementing user %s window width and step sizes" % field)

    if params.get(window_step, 0) == 0:
        params[window_step] = max(1, params[window_width] // 2)



def _set_thresholds(field, params, data, prefix=None, upper=False, freq=False, absmin=1):
    thresh = '%s_threshold' % field
    alpha = '%s_alpha' % field

    if params.get(alpha, 0) < 1:
        logging.info("  Estimating %s threshold" % field)

        if prefix is not None:
            prefix = "%s.%s" % (prefix, field)

        params[thresh] = _fitgamma(data[field],
                                   prefix=prefix,
                                   alpha=params[alpha],
                                   upper=upper,
                                   freq=freq)

        if params[thresh] < absmin:
            logging.warning(" Bad fit, estimating by integration")
            params[thresh] = _integrate(data[field],
                                        alpha=params[alpha],
                                        upper=upper)
    else:
        logging.info("  Implementing user %s threshold" % field)
        params[thresh] = int(params[alpha])

    if params[thresh] < absmin:
        logging.warning(" Estimated %s threshold too low, defaulting to %d" % (
            field, absmin))
        params[thresh] = absmin
        


def _set_contig_partitions(reference_info):
    return



def infer_parameters(reads, fasta, params=None, limit=_sample_limit, prefix=None):
    """
Parameter estimation by poisson/gamma distribution fitting
    """
    logging.info("Inferring parameters:")
    num_reads = 0
    reference_info = []
    longest_reference_name = None
    longest_reference_length = -1
    longest_reference_nreads = 0

    if params is None:
        params = {
            'span_alpha': _span_alpha_default,
            'term_alpha': _term_alpha_default,
            'freq_alpha': _freq_alpha_default,
            'span_window_step' : _span_ssize_default,
            'span_window_width': _span_wsize_default,
            'term_window_step' : _term_ssize_default,
            'term_window_width': _term_wsize_default,
            'min_align_length': _align_length_default,
        }

    for i in range(_num(fasta.references)):
        num_reads = 0
        try:
            reference_reads = reads.fetch(fasta.references[i])
        except ValueError:
            reference_reads = []

        for read in reference_reads:
            num_reads += 1
 
        if fasta.lengths[i] > longest_reference_length:
            longest_reference_name = fasta.references[i]
            longest_reference_length = fasta.lengths[i]
            longest_reference_nreads = num_reads

        reference_info.append((fasta.references[i], fasta.lengths[i], num_reads))

    
    _set_window_sizes('span', params, longest_reference_length, longest_reference_nreads, scale=4)

    if params['min_align_length']-2 < params['span_window_width']:
        params['min_align_length'] = params['span_window_width'] + 2    
    
    remaining_limit = limit
    large_window_data = {'span': [], 'freq': []}
    for reference_name, reference_length, n in sorted(reference_info, key=lambda x: -x[1]):
        if remaining_limit <= 0:
            break
        sampled_data = calculate_windowed_depth(
            reads, reference_name,
            0, reference_length, 
            params['span_window_width'],
            params['span_window_step'],
            params['min_align_length'],
            remaining_limit,
            counts=True
        )
        large_window_data['span'].extend(sampled_data['span'])
        large_window_data['freq'].extend(sampled_data['freq'])
        remaining_limit -= sampled_data['iter']
    
    _set_thresholds('span', params, large_window_data, prefix=prefix, absmin=_span_min, upper=False)
    _set_thresholds('freq', params, large_window_data, prefix=prefix, absmin=_freq_min, upper=False, freq=True)
    
    params['span_depth_mean'] = _mean(large_window_data['span'], default=0.0)
    params['span_depth_sdev'] = _sd(large_window_data['span'], mean=params['span_depth_mean'])


    _set_window_sizes('term', params, longest_reference_length, longest_reference_nreads, scale=1)

    small_window_data = {'term': []}
    if params.get('term_alpha', 0) < 1:
        remaining_limit = limit
        for reference_name, reference_length, n in sorted(reference_info, key=lambda x: -x[1]):
            if remaining_limit <= 0:
                break
            sampled_data = calculate_windowed_depth(
                reads, reference_name,
                0, reference_length, 
                params['term_window_width'],
                params['term_window_step'],
                params['min_align_length'],
                remaining_limit,
                counts=True
            )
            small_window_data['term'].extend(sampled_data['term'])
            remaining_limit -= sampled_data['iter']
        
    _set_thresholds('term', params, small_window_data, prefix=prefix, absmin=_term_min, upper=True)


    logging.info("  Inferred parameters:")
    logging.info("    min-align-length: %d" % (
        params['min_align_length']))
    logging.info("    span-width: %d" % (
        params['span_window_width']))
    logging.info("    span-step:  %d" % (
        params['span_window_step']))
    logging.info("    span-alpha: %s (=%s)" % (
        str(params['span_alpha']),
        str(params['span_threshold'])))
    logging.info("    freq-alpha: %s (=%s)" % (
        str(params['freq_alpha']),
        str(params['freq_threshold'])))
    logging.info("    term-width: %d" % (
        params['term_window_width']))
    logging.info("    term-step:  %d" % (
        params['term_window_step']))
    logging.info("    term-alpha: %s (=%s)" % (
        str(params['term_alpha']),
        str(params['term_threshold'])))
    

    return params



def _load_references_file(filename, indices, references):
    referencesfile = open(filename, 'r')
    for reference in referencesfile:
        reference = reference.rstrip().split('\t')[0]
        if reference in indices:
            references.add(reference)
        else:
            raise KeyError("Contig %r (via %s) not in reference file\n" % (
                reference, filename))

    referencesfile.close()
            

    
def load_references(fasta, referencelist):
    """Loads a list of reference names from the command-line or a file"""
    logging.debug("   Loading references")
    indices = dict(zip(fasta.references, range(_num(fasta.references))))

    references = set()
    if len(referencelist) < 1:
        return fasta.references

    for reference in referencelist:
        if reference in indices:
            references.add(reference)
        elif os.path.isfile(reference):
            _load_references_file(reference, indices, references)
        else:
            raise KeyError("Contig %r not in reference file\n" % reference)

    return sorted(list(references), key=indices.get)



def wombat(reads, fasta, contigs, records, params={}, references=[], notrim=False, nobreak=False):
    """Core working function"""
    references = load_references(fasta, references)

    span_window_width = params['span_window_width']
    span_window_step  = params['span_window_step']
    term_window_width = params['term_window_width']
    term_window_step  = params['term_window_step']
    min_alignment_length = params['min_align_length']
    
    logging.info("Identifying reference breaks:")

    records.write(_track_header)

    if logging.getLogger().level == logging.DEBUG:
        debugfile = open('wombat.debug.bed', 'w')

    for reference_name in references:
        reference_sequence = fasta.fetch(reference_name).upper()
        reference_length = len(reference_sequence)
        curr_break_start = reference_length
        prev_break_start = -1
        prev_break_end = -1
        prev_clear_start = -1
        prev_clear_end = -1
        prev_trim_start = -1
        prev_trim_end = -1
        trim_buff_start = -1
        trim_buff_end = -1
        open_break = False


        logging.info("  Traversing %s" % reference_name)
        span_windows = calculate_windowed_depth(
            reads, reference_name,
            0, reference_length, 
            span_window_width,
            span_window_step,
            min_alignment_length
        )

        if logging.getLogger().level == logging.DEBUG:
            for window in span_windows:
                debugfile.write(_debug_record(
                    reference_name,
                    window[0],  # window_start
                    window[1],  # window_end
                    window[2],  # num_ends
                    window[3],  # num_spans
                    window[4],  # num_starts,
                    window[5])  # frq_spans
                )


        span_depth_mean = _mean(span_windows, default=0.0, key=lambda x: x[3])
        span_depth_dev = _sd(span_windows, default=0.0, key=lambda x: x[3])
        term_thresh = max(_term_min, params['term_threshold'])        
        span_thresh = max(_span_min, params['span_threshold'] * \
                          (span_depth_mean / params['span_depth_mean']))
        freq_thresh = max(_freq_min, params['freq_threshold'])
        
        # Calculate the regions of the contig to be trimmed due to low
        # read depth. This is necessary because OLC assemblers may allow
        # one or a few dangling reads at the contig termini.

        for span_window in span_windows:
            if span_window[3] >= span_thresh:
                if prev_trim_start <  0:
                    prev_trim_start = span_window[0]
                if prev_trim_start >= 0:
                    prev_trim_end = span_window[1]

            if span_window[5] >= freq_thresh:
                if trim_buff_start <  0:
                    trim_buff_start = span_window[0]
                if trim_buff_start >= 0:
                    trim_buff_end = span_window[1]

        trim_buff_start = max(prev_trim_start, trim_buff_start)
        trim_buff_end = min(prev_trim_end, trim_buff_end)

        # prev_trim_start will equal span_window_width when trimming is not
        # necessary (0 spanning coverage at start of contig), so force
        # back to 0.
        if prev_trim_start <= span_window_step:
            prev_trim_start = 0
            
        # prev_trim_end will be less than or equal to span_window_width when
        # trimming is not necessary (0 spanning coverage at end of contig),
        # so force back to reference_length
        if prev_trim_end <= span_window_step:
            prev_trim_end = reference_length
            
        if reference_length - prev_trim_end <= span_window_step:
            prev_trim_end = reference_length

        # if our average contig depth is less than our ability to resolve
        # low depth regions, as it often is for small contigs, we'll be 
        # unnecessarily trimming them. Prevent this from occurring.

        if notrim or span_depth_mean <= span_thresh:
            prev_trim_start = 0
            prev_trim_end = reference_length

        logging.debug("   Spanning depth: mean=%0.1f, sd=%0.3f" % (
            span_depth_mean, span_depth_dev))

        # Write the 5' trim region only if trimming is necessary
        logging.debug("   Trimming contig: %s" % ( 
            str((reference_name, reference_length, prev_trim_start, prev_trim_end))))

        if prev_trim_start > 0:
            records.write(_trim_record(
                reference_name,
                0, 
                prev_trim_start)
            )

        prev_clear_start = prev_trim_start

        if nobreak:
            prev_clear_end = prev_trim_end

        else:
            # Most (but not all) contigging errors can be identified 
            # with an excess of raw read starts (or read ends) and low
            # spanning coverage. The following loop attempts to identify
            # these regions specifically while also being robust against
            # random fluctuations in read depth:
        
            span_window_depth = 0
            span_window_count = 0
            for span_window in span_windows:
                # First check that we aren't calling contigging misassembly
                # breaks within or adjacent to the 5' and 3' trimmed regions
                if span_window[0] <= trim_buff_start:
                    logging.debug("   Window (%d, %d): overlaps trim, continue" % (
                        span_window[0], span_window[1]))
                    continue
                if span_window[1] >= trim_buff_end:
                    logging.debug("   Window (%d, %d): overlaps trim, continue" % (
                        span_window[0], span_window[1]))
                    continue

                # If the current window position contains low spanning 
                # read depth, use a smaller window to determine if there
                # is a non-random pattern of read starts/ends. Using a
                # second, smaller window size incurs a performance penalty
                # but improves our ability to discern high starts/ends due
                # to randomness (false positives) vs misassemblies (true 
                # positives) for a given set of parameters.
                #
                # For example, a window containing 4 starts and 3 ends:
                #    v-------------v <---[spanning read window]
                #    :      v---v <------[read termini window]
                #    :      :   :  : 
                #    :   ===-===-===...  
                # ...=======-===-= =...
                # ...=======-= =-===... <[random read starts/ends]
                # ...==== ==-===-===...
                #    :      :   :  :
                # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                #    :      :   :  : 
                #    :      :  =-===...
                # ...=======-= =-===... <[patterned read starts/ends]
                # ...=======-= =-===...
                # ...=======-= =-===...
                #             ^----------[location of misassembly]
                #
                span_fails = False
                term_fails = False
                if span_window[3] < span_thresh and span_window[5] < freq_thresh:
                    span_fails = True
                    term_windows = calculate_windowed_depth(
                        reads, reference_name,
                        span_window[0], span_window[1],
                        term_window_width, term_window_step,
                        min_alignment_length
                    )
                    for term_window in term_windows:
                        if term_window[2] > term_thresh or term_window[4] > term_thresh:
                            term_fails = True
                            break

                logging.debug("   Window (%d, %d): span_fails=%s, term_fails=%s" % (
                    span_window[0], span_window[1], str(span_fails), str(term_fails)))


                if span_fails and term_fails:
                    if span_window[0] <= prev_break_end:
                        # The current window overlaps a previously identified
                        # break (the previous window also contained a break),
                        # extend the break region.
                        logging.debug("   Window (%d, %d): extending break: overlap" % (
                            span_window[0], span_window[1]))
                        prev_break_end = span_window[1]

                    elif prev_break_start > 0:
                        # prev_break_start will be greater than `0' if we've
                        # encountered a break upstream of the current window.
                        # We cannot report the upstream break window just yet,
                        # however, as it may be the left side of a low-depth
                        # valley (a region of few reads flanked by many hard
                        # starts and stops) and the current window discovers
                        # the right side of that valley. In this case, we
                        # want to omit the whole intervening region.
                        if span_window_depth // max(1, span_window_count) < span_thresh:
                            # low-depth valley, extend previous break.
                            #          v---v <-----------[low depth valley]
                            # ...==== =     ==== ==...
                            # ...=== ==     === ===...
                            # ...= ====     == ====...
                            # ...=== ========== ===...
                            #          ^---^ <-----------[break region]
                            #
                            logging.debug("   Window (%d, %d): extending break: depth valley" % (
                                span_window[0], span_window[1]))
                            prev_break_end = span_window[1]

                        else:
                            # We've encountered a new, distant break. Report the
                            # previous break and start anew.
                            logging.debug("   Window (%d, %d): reporting break" % (
                                span_window[0], span_window[1]))
                            records.write(_clear_record(
                                reference_name, 
                                prev_clear_start, 
                                prev_break_start)
                            )
                            records.write(_break_record(
                                reference_name, 
                                prev_break_start, 
                                prev_break_end)
                            )
                            contigs.write(_fasta_record(
                                _region_string(
                                    reference_name, 
                                    prev_clear_start+1, 
                                    prev_break_start), 
                                reference_sequence[prev_clear_start:prev_break_start], 
                                width=100)
                            )

                            prev_clear_start = prev_break_end
                            prev_break_start = min(curr_break_start, span_window[0])
                            prev_break_end = span_window[1]

                    else:
                        # First break of the contig encountered, record positions
                        # and do nothing.
                        logging.debug("   Window (%d, %d): initial break" % (
                            span_window[0], span_window[1]))
                        prev_break_start = span_window[0]
                        prev_break_end = span_window[1]

                    span_window_depth = 0
                    span_window_count = 0
                    open_break = True
            
                else:
                    # The following conditionals handle cases where only one
                    # side of the misassembly has an excess of starts (in these
                    # cases there is usually a valley preceeding or following
                    # the region triggering the break)
                    if open_break and span_window[5] >= freq_thresh:
                        # The break was triggered by an excess of read ends and
                        # read starts taper as coverage increases
                        #                v-----------+
                        # ...= ====      ====...     |minimum spanning depth|
                        # ...== ===    ======...     |encountered here again|
                        # ...=== ==   =======...
                        # ...============ ===...
                        #          ^----^ <----------[break region]
                        #
                        logging.debug("   Window (%d, %d): extending break: hard stops, tapered starts" % (
                            span_window[0], span_window[1]))
                        prev_break_end = span_window[0]
                        open_break = False

                    if span_window[5] > freq_thresh:
                        # The break was triggered by an excess of read starts
                        # and read ends taper as coverage decreases
                        #      v----------------------+
                        # ...===      ====== =...     |last minimum spanning|
                        # ...= ===    ===== ==...     |depth region observed|
                        # ...=== ===  ==== ===...     
                        # ...===== ====== ====...     
                        #       ^----^ <--------------[break region]
                        #
                        logging.debug("   Window (%d, %d): extending %s" % (
                            span_window[0], span_window[1], 'break' if open_break else 'clear'))
                        curr_break_start = span_window[1]

                span_window_depth += span_window[3]
                span_window_count += 1

            
            # If the last observed break doesn't intersect the prev_trim_end
            # write it. This prevents messy overlapping BED records at the ends
            if trim_buff_start < prev_break_start and prev_break_end < prev_trim_end:
                logging.debug("   Window (%d, %d): reporting break" % (
                    span_window[0], span_window[1]))
                records.write(_clear_record(
                    reference_name, 
                    prev_clear_start,
                    prev_break_start)
                )
                records.write(_break_record(
                    reference_name, 
                    prev_break_start,
                    prev_break_end)
                )
                contigs.write(_fasta_record(
                    _region_string(
                        reference_name, 
                        prev_clear_start+1, 
                        prev_break_start), 
                    reference_sequence[prev_clear_start:prev_break_start], 
                    width=100)
                )

                prev_clear_start = prev_break_end


        # Write the last clear region
        records.write(_clear_record(
            reference_name,
            prev_clear_start,
            prev_trim_end)
        )
        contigs.write(_fasta_record(
            _region_string(
                reference_name, 
                prev_clear_start+1, 
                prev_trim_end),
            reference_sequence[prev_clear_start:prev_trim_end],
            width=100)
        )

        # Write the 3' trim region only if trimming is necessary
        if prev_trim_end < reference_length:
            records.write(_trim_record(
                reference_name,
                prev_trim_end,
                reference_length)
            )
            
        contigs.flush()
        records.flush()

    if logging.getLogger().level == logging.DEBUG:
        debugfile.close()



def _report_module_versions():
    program = __program__.upper()
    logging.info("%s %s" % (program, __version__))
    
    logging.info("Python v%s" % (sys.version.replace('\n',' ')))
    if sys.version_info < (2, 7, 0):
        logging.warning("%s untested with Python older than v2.7.0" % (
            program,))

    logging.info("pySAM v%s" % pysam.__version__)
    if tuple(map(int, pysam.__version__.split('.'))) < (0, 8, 4):
        logging.warning("%s untested with pySAM older than v0.8.4" % (
            program,))

    logging.info("SciPy v%s" % scipy.__version__)
    if tuple(map(int, scipy.__version__.split('.'))) < (1, 0, 0):
        logging.warning("%s untested with SciPy older than v1.0.0" % (
            program,))

        

def version():
    sys.stderr.write("%s %s\n" % (__program__.upper(), __version__))
    sys.exit(0)

    

def usage(message=None, full=False, exitcode=1):
    message = '\n' if message is None else '\nError: %s\n\n' % message

    if full:
        sys.stderr.write("\n")
        sys.stderr.write("Program: %s (Weak Overlap Mis-assembly Breaking And Trimming)\n" % (
            __program__.upper()))
        sys.stderr.write("Version: %s\n" % __version__)
        sys.stderr.write("Contact: %s\n" % __author__)
        sys.stderr.write("\n")
        sys.stderr.write("Usage: %s [options] <molecules.bed.gz> <reference.fasta> <out_prefix>\n" % (
            __program__))
        sys.stderr.write(main.__doc__ + message)
        sys.exit(exitcode)

    else:
        sys.stderr.write("Usage: %s [-BDFThv] \\\n" % __program__)
        sys.stderr.write("        [-A<uint>] [-a<uint>] [-L<str>|<file>] [-l<uint>] \\\n")
        sys.stderr.write("        [-W<uint>] [-w<uint>] [-S<uint>] [-s<uint>] [-m<uint>] \\\n")
        sys.stderr.write("        <molecules.bed.gz> <reference.fasta> <out_prefix>\n")
        sys.stderr.write("\n")
        sys.stderr.write("Notes: Use `--help' for more information\n%s" % message)
        sys.exit(exitcode)



def main(argv):
    """
Options:
  -L,--list <str>|<file>       Contig name or .list file of contig names,
                               listing one contig per line. May be specified
                               zero or more times. default: all contigs

  -l,--sample-limit <uint>     Limit param inference to the first N windows
                               on the longest contig. default: all

  -A,--span-alpha <float>      Lower-tail alpha for the number of molecules
                               spanning the target window. Windows with
                               spanning depth below this value trigger term
                               window scanning. default: 0.01

  -a,--term-alpha <float>      Upper-tail alpha for the number of molecules
                               starting/ending within the target window.
                               Regions with excess starts/ends are annotated
                               for breaking. default: 0.01

  -W,--span-width <uint>       Sliding window width for counting spanning
                               molecules. 1kb or larger recommended.
                               default: auto

  -S,--span-step <uint>        Sliding window step size for counting spanning
                               molecules. default: 0.5 * span-width

  -w,--term-width <uint>       Sliding window width for counting molecule 
                               starts/ends. 100 bp or smaller recommended.
                               default: auto

  -s,--term-step <uint>        Sliding window step size for counting molecule
                               starts/ends. default: 0.5 * term-width
                            
  -m,--min-align-length <uint> Minimum alignment length to consider to count
                               toward spans and terms. Value must be greater
                               than span-width. default: auto

  -F,--fit-only                Only perform parameter estimation by fitting
                               and then exit.

  -B,--no-break                Do not perform any contig breaking

  -T,--no-trim                 Do not perform any contig trimming

  -D,--debug                   Run in debugging mode (very verbose)

  -h,--help                    Print this help documentation and exit

  -v,--version                 Print the version and exit

Outputs:
  out_prefix.span.tsv          Spanning molecule counts used by parameter
                               estimation procedure to determine -A value

  out_prefix.span.pdf          Spanning molecule count histogram with fit
                               line parameterized by estimated values

  out_prefix.freq.tsv          Spanning molecule frequency used by param-
                               eters estimation procedure to determine 
                               -f value
  
  out_prefix.freq.pdf          Spanning molecule fraction histogram with 
                               fit line parameterized by estimated values

  out_prefix.term.tsv          Molecule termini counts used by parameter
                               estimation procedure to determine -a value

  out_prefix.term.pdf          Molecule termini count histogram with fit
                               line parameterized by estimated values

  out_prefix.fasta             FASTA file of broken and trimmed contigs

  out_prefix.bed               BED file of breaks, trims, and clear 
                               regions formatted for viewing in IGV
    
Notes:
  reference.fasta contains contigs and is indexed with samtools faidx.
  molecule.bed.gz is a bgzip-compressed and tabix-indexed BED file of
  aligned molecules: If PacBio/ONT long reads, the BED records represent
  longest single subreads (only 10kb+ recommended); if 10X Genomics data,
  each BED record must represent the extents of each GEM-barcoded molecule
  as mapped to the reference, not the individual linked reads; if mate-
  pairs (experimental), each BED record represents the outer-coordinates
  of the mapped molecule/insert.

  The --term-alpha and --span-alpha options are used to estimate molecule
  count thresholds for significant deviations from the expected number of
  molecules starting/ending and spanning an interval window, respectively,
  based on their fitted distributions. If these options are set to values
  greater than or equal to 1.0, no empirical estimation is performed and
  those values are used directly as the maximum starting/ending and min-
  imum spanning molecule count thresholds. This is useful if the parameter
  estimation procedure fails to determine reasonable values because of
  poorly fit data.
  
    """
#---+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
#       10        20        30        40        50        60        70        80

    #TODO: Add a `--scaffold` flag that breaks scaffolds into virtual contigs
    #TODO: Generalize the parameter estimation to work on `--scaffold` contigs
    #TODO: Add a `--mask` option that masks instead of breaks
    #TODO: Add `--preset` flag with enumerative pb, 10X, and mp presets
    #TODO: Add config file param loading
    long_options = (
        'help',
        'debug',
        'version',
        'no-trim',
        'no-break',
        'fit-only',
        'preset=',
        'span-width=',
        'span-step=',
        'term-width=',
        'term-step=',
        'term-alpha=',
        'span-alpha=',
        'freq-alpha=',
        'sample-limit=',
        'min-align-length=',
        'list=',
    )
    try:
        options, arguments = getopt.getopt(argv, 'BDFThvW:w:S:s:L:l:a:A:f:m:', long_options)
    except getopt.GetoptError as error:
        usage(error)

    limit = _sample_limit
    debug = False
    params = {}
    notrim = False
    nobreak = False
    fitonly = False
    references = []
    referenceslist = []
    params['span_alpha'] = _span_alpha_default
    params['term_alpha'] = _term_alpha_default
    params['freq_alpha'] = _freq_alpha_default
    params['span_window_step']  = _span_ssize_default
    params['span_window_width'] = _span_wsize_default
    params['term_window_step']  = _term_ssize_default
    params['term_window_width'] = _term_wsize_default
    params['min_align_length'] = _align_length_default
    for flag, value in options:
        try:
            if   flag in {'-L','--list'}: referenceslist.append(str(value))
            elif flag in {'-l','--sample-limit'}: limit = int(value)
            elif flag in {'-a','--term-alpha'}: params['term_alpha'] = float(value)
            elif flag in {'-A','--span-alpha'}: params['span_alpha'] = float(value)
            elif flag in {'-f','--freq-alpha'}: params['freq_alpha'] = float(value)
            elif flag in {'-W','--span-width'}: params['span_window_width'] = int(value)
            elif flag in {'-S','--span-step'}: params['span_window_step'] = int(value)
            elif flag in {'-w','--term-width'}: params['term_window_width'] = int(value)
            elif flag in {'-s','--term-step'}: params['term_window_step'] = int(value)
            elif flag in {'-m','--min-align-length'}: params['min_align_length'] = int(value)
            elif flag in {'-h','-H','--help'}: usage(full=(flag=='--help'))
            elif flag in {'-F','--fit-only'}: fitonly = True
            elif flag in {'-B','--no-break'}: nobreak = True
            elif flag in {'-T','--no-trim'}: notrim = True
            elif flag in {'-D','--debug'}: debug = True
            elif flag in {'-v','--version'}: version()
        except ValueError:
            usage("Non-numeric value for %r: %r" % (flag, value))

    if debug:
        logging.getLogger().setLevel(logging.DEBUG)        
    else:
        sys.excepthook = _log_exception
        
    if _num(arguments) != 3:
        usage('Unexpected number of arguments')
    if params['term_alpha'] < 0:
        usage("Term alpha {-1} must be non-negative float")
    if params['span_alpha'] < 0:
        usage("Span alpha {-A} must be non-negative float")
    if params['span_window_width'] < 0:
        usage("Window width {-W} must be positive integer")
    if params['term_window_width'] < 0:
        usage("Window width {-w} must be positive integer")
    if params['span_window_width'] < params['term_window_width']:
        usage("Span window width {-W} must be greater than term window width {-w}")
    if params['span_window_step'] < 0:
        usage("Window step size {-S} must be positive integer")
    if params['term_window_step'] < 0:
        usage("Window step size {-s} must be positive integer")
    if params['span_window_width'] < params['span_window_step']:
        usage("Window step size {-S} must be less than width {-w}")
    if params['term_window_width'] < params['term_window_step']:
        usage("Window step size {-s} must be less than width {-w}")
    if params['span_window_step'] < params['term_window_step']:
        usage("Span window step {-S} must be greater than term window step {-s}")
    if params['min_align_length']-2 < params['span_window_width']:
        usage("Minimum alignment length {-m} must be greater than or equal to 2 + span window width {-w}")
        
    _report_module_versions()

    logging.info("Command: %s %s" % (__program__, ' '.join(argv)))
    logging.debug(" Running in (very verbose) debugging mode")
    
    fasta  = pysam.FastaFile(arguments[1])
    reads  = pysam.TabixFile(arguments[0], parser=pysam.asBed())
    params = infer_parameters(reads, fasta, params=params, limit=limit, prefix=arguments[2])

    if fitonly:
        logging.info("Finished")
        return 0

    contigs = open(arguments[2]+'.fasta', 'w')
    records = open(arguments[2]+'.bed', 'w')

    wombat(reads, fasta, contigs, records, params, referenceslist, notrim=notrim, nobreak=nobreak)

    contigs.close()
    records.close()

    logging.info("Finished")

    return 0

    

if __name__ == '__main__':
    main(sys.argv[1:])
