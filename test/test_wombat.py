import unittest
import wombat

# class TestFunction_ceil(unittest.TestCase):
#     def test_positiveInteger(self):
#         self.assertEqual(wombat._ceil(1), 1)

#     def test_positiveFloat(self):
#         self.assertEqual(wombat._ceil(0.1), 1.0)

#     def test_negativeInteger(self):
#         self.assertEqual(wombat._ceil(-1), -1)

#     def test_negativeFloat(self):
#         self.assertEqual(wombat._ceil(-0.5), 0.0)


class TestFunction_mean(unittest.TestCase):
    def test_nonEmpty1(self):
        # using self.assertIs() or self.assertIsNone() is not python2.6 compatible
        self.assertEqual(wombat._mean([]), None)

    def test_nonEmpty2(self):
        self.assertEqual(wombat._mean([], default=0.0), 0.0)        
        
    def test_integers1(self):
        self.assertEqual(wombat._mean([1]), 1.0)

    def test_integers2(self):
        self.assertEqual(wombat._mean([1, 1]), 1.0)

    def test_integers3(self):
        self.assertEqual(wombat._mean([1, 2]), 1.5)        
        
    def test_integers4(self):
        self.assertEqual(wombat._mean([1, 2, 3]), 2.0)

    def test_float1(self):
        self.assertEqual(wombat._mean([5.3]), 5.3)

    def test_float2(self):
        self.assertEqual(wombat._mean([1.1, 3.4]), 2.25)

    def test_float3(self):
        self.assertEqual(wombat._mean([-0.1, 0.0, 0.1]), 0.0)        
        
    
class TestFunction_sd(unittest.TestCase):
    def test_nonEmpty1(self):
        self.assertEqual(wombat._sd([]), None)

    def test_nonEmpty2(self):
        self.assertEqual(wombat._sd([], default=0.0), 0.0)        
        
    def test_integers1(self):
        self.assertEqual(wombat._sd([1]), 0.0)

    def test_integers2(self):
        self.assertEqual(wombat._sd([1,1]), 0.0)

    def test_integers3(self):
        self.assertEqual(wombat._sd([-1,0,1]), 1.0)

    def test_float1(self):
        self.assertEqual(wombat._sd([-1.0, 0.0, 1.0]), 1.0)


class TestFunction_fold(unittest.TestCase):
    def test_notFolded1(self):
        self.assertEqual(wombat._fold('ATCGTACTAGCTGAC', width=None), 'ATCGTACTAGCTGAC')

    def test_notFolded2(self):
        self.assertEqual(wombat._fold('ATCGTACTAGCTGAC', width=20), 'ATCGTACTAGCTGAC')

    def test_isFolded1(self):
        self.assertEqual(wombat._fold('ATCGTACTAGCTG', width=5), 'ATCGT\nACTAG\nCTG')
        
    def test_isFolded2(self):
        # Strings with length a multiple of the width should not end in '\n'
        self.assertEqual(wombat._fold('ATCGTACTAGCTGAC', width=5), 'ATCGT\nACTAG\nCTGAC')



